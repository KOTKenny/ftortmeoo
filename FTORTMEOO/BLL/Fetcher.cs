﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTORTMEOO.BLL
{
    public static class Fetcher
    {
        public static string FetchToUnits(double number)
        {
            var count = 0;

            while(number >= 10)
            {
                number /= 10;
                count++;
            }

            var upperNumbersList = new List<string>();
            var postfixString = "10";

            while(count >= 1)
            {
                upperNumbersList.Add(Constants.UpperNumbers[count % 10]);
                count /= 10;
            }

            for (int i = upperNumbersList.Count - 1; i >= 0; i--)
            {
                postfixString += upperNumbersList[i];
            }

            return String.Format("{0} * {1}", number.ToString("n2"), postfixString);
        }
    }
}
