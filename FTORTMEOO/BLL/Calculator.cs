﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTORTMEOO.BLL
{
    public static class Calculator
    {
        public static double CalculateGasParticleArea(decimal P, int T)
        {
            return (2.687 * Math.Pow(10, 19) * Convert.ToDouble(Converter.ConvertPascalToMillimeterOfMercury(P)) * 273) / (760 * Convert.ToDouble(T));
        }

        public static double CalculateEffectiveFrequency(double a, decimal Te, double Nm)
        {
            return 8.3 * Math.Pow(10, 5) * Math.PI * Math.Pow(a, 2) * Math.Sqrt(Convert.ToDouble(Te)) * Nm;
        }

        public static double CalculateElectronMobility(double V)
        {
            return (1.76 * Math.Pow(10, 15)) / V;
        }

        public static double CalculateCurrentDensity(decimal A)
        {
            return Convert.ToDouble(A / 1000);
        }

        public static double CalculateElectronConcentration(double Je, int E, double e, double MU)
        {
            return (Je / E) / (e * MU);
        }
    }
}
