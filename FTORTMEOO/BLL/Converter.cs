﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FTORTMEOO.BLL
{
    public static class Converter
    {
        public static decimal ConvertPascalToMillimeterOfMercury(decimal P)
        {
            const decimal Coeficient = 133.33M;

            return P / Coeficient;
        }

        public static DataTable GridViewToDataTable(DataGridView gridView)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < gridView.Columns.Count; i++)
            {
                dt.Columns.Add(gridView.Columns[i].HeaderText);
            }
            foreach (DataGridViewRow row in gridView.Rows)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < gridView.Columns.Count; j++)
                {
                    dr[gridView.Columns[j].HeaderText] = row.Cells[j].FormattedValue;
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
    }
}
