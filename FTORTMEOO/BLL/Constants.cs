﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTORTMEOO.BLL
{
    public static class Constants
    {
        public const int T = 293;
        public static double a_air = 1.2 * Math.Pow(10, -8);
        public const int E = 50;
        public static double e = 1.6 * Math.Pow(10, -19);

        public static string[] UpperNumbers = { "\u2070", "\u00B9", "\u00B2", "\u00B3", "\u2074", "\u2075",
                                                "\u2076", "\u2077", "\u2078", "\u2079"};
    }
}
