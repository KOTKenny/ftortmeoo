﻿using ClosedXML.Excel;
using FTORTMEOO.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Xceed.Words.NET;

namespace FTORTMEOO
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            decimal P = 0;
            decimal I = 0;
            decimal Te = 0;

            try
            {
                P = Convert.ToDecimal(input_P.Text);
                I = Convert.ToDecimal(input_I.Text);
                Te = Convert.ToDecimal(input_Te.Text);

                lblError.Visible = false;
            }
            catch (Exception ex) {
                lblError.Visible = true;
                return;
            }

            var Je = Calculator.CalculateCurrentDensity(I);
            var Nm = Calculator.CalculateGasParticleArea(P, Constants.T);
            var V = Calculator.CalculateEffectiveFrequency(Constants.a_air, Te, Nm);
            var Mu = Calculator.CalculateElectronMobility(V);
            var Ne = Calculator.CalculateElectronConcentration(Je, Constants.E, Constants.e, Mu);

            dataGridView1.Rows.Add(I, P, Te, Je, Fetcher.FetchToUnits(Nm), Fetcher.FetchToUnits(V), Fetcher.FetchToUnits(Mu), Fetcher.FetchToUnits(Ne));
            chart1.Series[0].Points.AddXY(P, Ne);

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            chart1.Series[0].Points.Clear();
        }

        protected void Chart1_Customize(object sender, EventArgs e)
        {
            foreach (var lbl in chart1.ChartAreas[0].AxisY.CustomLabels)
            {
                lbl.Text = Fetcher.FetchToUnits(double.Parse(lbl.Text));
            }
        }

        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using(SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx" })
            {
                if(sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        using(XLWorkbook workbook = new XLWorkbook())
                        {
                            workbook.Worksheets.Add(Converter.GridViewToDataTable(this.dataGridView1), "Data");
                            workbook.SaveAs(sfd.FileName);
                        }

                        MessageBox.Show("Данные экспортированы успешно!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch(Exception ex){
                        MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void docToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileName = String.Format("{0}\\Files\\example.docx", Directory.GetCurrentDirectory());

            var doc = DocX.Load(fileName);

            var list = doc.FindAll("ТаблицаПолученныхДанных");

            //Table Replace
            var placeholderTable = doc.Tables[doc.Tables.Count - 1];

            var realTable = placeholderTable.InsertTableAfterSelf(dataGridView1.Rows.Count, dataGridView1.Columns.Count);

            placeholderTable.Remove();

            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                realTable.Rows[0].Cells[i].Paragraphs.First().Append(dataGridView1.Columns[i].HeaderText);
            }
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    realTable.Rows[i + 1].Cells[j].Paragraphs.First().Append(dataGridView1.Rows[i].Cells[j].FormattedValue.ToString());
                }
            }

            var paragraphForChart = doc.Paragraphs.First(p => p.Text.Contains("концентрации электронов от давления"));
            using (var chartimage = new MemoryStream())
            {
                chart1.SaveImage(chartimage, ChartImageFormat.Png);
                var image = doc.AddImage(chartimage);
                var picture = image.CreatePicture();
                picture.Width = 507F;
                paragraphForChart.AppendPicture(picture);
            }

            using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Doc files|*.docx" })
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {

                        doc.SaveAs(sfd.FileName);

                        MessageBox.Show("Файл сохранён!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
